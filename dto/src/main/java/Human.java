import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Objects;

public class Human {

    public String firstName;
    public String lastName;
    public String patronymic;
    public Integer age;
    public String male;
    public Date date;

    public Human(String firstName, String lastName, String patronymic, Integer age, String male, Date date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.age = age;
        this.male = male;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(patronymic, human.patronymic) &&
                Objects.equals(male, human.male) &&
                Objects.equals(date, human.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, patronymic, age, male, date);
    }
}
