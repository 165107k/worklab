
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Method {


    public boolean firstMethod(LinkedList<Human> humanList, Human human){
        int matches = 0;
        boolean m;
        for(int i =0; humanList.size() > i; i++){
            if(humanList.get(i).equals(human)){
                matches++;
            }
        }
        if(matches > 0){
            System.out.println("Больше одного совпадения");
            m = true;
        }else {
            System.out.println("Совпадений нет");
            m = false;
        }
        return m;
    }

    public LinkedList<Human> secondMethod(LinkedList<Human> humanList){
        boolean m;
        LinkedList<Human> r = new LinkedList<>();
        for(int i =0; humanList.size() > i; i++){
            if(humanList.get(i).age < 20 &&
                    (humanList.get(i).lastName.charAt(0) == 'а' || humanList.get(i).lastName.charAt(0) == 'A' ||
                            humanList.get(i).lastName.charAt(0) == 'б' || humanList.get(i).lastName.charAt(0) == 'Б' ||
                            humanList.get(i).lastName.charAt(0) == 'в' || humanList.get(i).lastName.charAt(0) == 'В' ||
                            humanList.get(i).lastName.charAt(0) == 'г' || humanList.get(i).lastName.charAt(0) == 'Г' ||
                            humanList.get(i).lastName.charAt(0) == 'д' || humanList.get(i).lastName.charAt(0) == 'Д')){
                System.out.println(humanList.get(i).firstName + " " + humanList.get(i).lastName);
                r.add(humanList.get(i));
            }
        }
        return r;
    }

    public HashMap<Human, Integer> thirdMethod(LinkedList<Human> humanList) {

        HashMap<Human, Integer> mapHuman = new HashMap<>();
        for (int i = 0; i < humanList.size(); i++) {
            int matches = 0;

            for (int j = i+1; j < humanList.size(); j++) {
                if(humanList.get(i).equals(humanList.get(j))){
                    matches++;
                }
            }
            if(mapHuman.get(humanList.get(i)) == null){
                mapHuman.put(humanList.get(i), matches);
            }
        }
        for (Map.Entry<Human, Integer> entry : mapHuman.entrySet()) {
            System.out.println("{" + (entry.getValue()+1) + ":" + "[" + "{" + entry.getKey().firstName + " " +
                    entry.getKey().lastName + " " +
                    entry.getKey().patronymic + " " +
                    entry.getKey().age + " " +
                    entry.getKey().male + " " +
                    entry.getKey().date + "}" + "}" + "]");
        }
        return mapHuman;
    }

}