
import java.util.Date;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        Date date = new Date(880761600000L);

        Human template = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);

        Human human1 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);
        Human human2 = new Human("Сергей", "бучкин", "Сергеевич", 19, "M", date);
        Human human3 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);
        Human human4 = new Human("Сергей", "бучкин", "Сергеевич", 19, "M", date);
        Human human5 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);

        LinkedList<Human> listHuman = new LinkedList<>();
        listHuman.add(human1);
        listHuman.add(human2);
        listHuman.add(human3);
        listHuman.add(human4);
        listHuman.add(human5);

        Method method = new Method();

        System.out.println("ПЕРВЫЙ МЕТОД");
        method.firstMethod(listHuman, template);
        System.out.println("ВТОРОЙ МЕТОД");
        method.secondMethod(listHuman);
        System.out.println("ТРЕТИЙ МЕТОД");
        method.thirdMethod(listHuman);

    }
}
