import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static org.junit.Assert.*;

public class MethodTest {
    Date date = new Date(880761600000L);

    Human template = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);

    Human human1 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);
    Human human2 = new Human("Сергей", "бучкин", "Сергеевич", 19, "M", date);
    Human human3 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);
    Human human4 = new Human("Сергей", "бучкин", "Сергеевич", 19, "M", date);
    Human human5 = new Human("Дмитрий", "бучкин", "Сергеевич", 19, "M", date);

    LinkedList<Human> listHuman = new LinkedList<>();

    @Test
    public void firstMethod() {
        listHuman.add(human1);
        listHuman.add(human2);
        listHuman.add(human3);
        listHuman.add(human4);
        listHuman.add(human5);

        Method method = new Method();
        boolean actual = method.firstMethod(listHuman, template);

        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void secondMethod() {
        listHuman.add(human1);
        listHuman.add(human2);
        listHuman.add(human3);
        listHuman.add(human4);
        listHuman.add(human5);

        Method method = new Method();
        LinkedList<Human> actual = method.secondMethod(listHuman);

        LinkedList<Human> r = new LinkedList<>();
        r.add(human1);
        r.add(human2);
        r.add(human3);
        r.add(human4);
        r.add(human5);

        LinkedList<Human> expected = r;
        assertEquals(expected, actual);
    }

    @Test
    public void thirdMethod() {
        listHuman.add(human1);
        listHuman.add(human2);
        listHuman.add(human3);
        listHuman.add(human4);
        listHuman.add(human5);

        Method method = new Method();

        HashMap<Human, Integer> actual = method.thirdMethod(listHuman);

        HashMap<Human, Integer> expected = new HashMap<>();
        expected.put(human1, 2);
        expected.put(human2, 1);

        assertEquals(expected, actual);
    }
}